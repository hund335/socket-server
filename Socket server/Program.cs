﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Socket_server
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Title = "Server #" + IPAddress.Loopback;
            Console.WriteLine("Starting the server...");

            TcpListener server = new TcpListener(IPAddress.Loopback, 8888);
            server.Start();
            TcpClient client = server.AcceptTcpClient();
            
            while ((true))
            {

                NetworkStream stream = client.GetStream();

                ArrayList al = new ArrayList();
                
                Byte[] bytes = new Byte[client.ReceiveBufferSize];
                stream.Read(bytes, 0, (int)client.ReceiveBufferSize);
                String clientdata = System.Text.Encoding.ASCII.GetString(bytes);
                clientdata = clientdata.Substring(0, clientdata.IndexOf("$"));

                stream.Flush();
                Console.WriteLine("Client > " + clientdata);
            
            }
           
        }
    }
}
